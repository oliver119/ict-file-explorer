package ict.com.explorer.ui;

public interface ScrollDirectionListener {
    void onScrollDown();
    void onScrollUp();
}